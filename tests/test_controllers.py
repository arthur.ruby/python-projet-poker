"""Application controllers tests."""
import unittest

from controllers import Game
from models import Player, Deck, Table


class GameControllerTest(unittest.TestCase):
    """ Class that handles the Game controller unit tests."""

    TEST_SEED = 42

    def setUp(self) -> None:
        self.game = Game(seed=self.TEST_SEED, player_name="Jean-Barnabé")

    def test_deck_shuffle(self):
        """Tests that it's able to shuffle the deck of cards."""
        another_deck = Deck(seed=self.TEST_SEED)
        self.assertNotEqual(another_deck, self.game.shuffle_deck())

    def test_initial_deals_two_cards_per_player(self):
        """Tests that the app deals 2 cards to each player."""
        self.game.deal_initial_round()
        for player in self.game.players:
            self.assertEqual(Player.NUMBER_OF_CARDS_IN_PLAYERS_HAND, len(player.hand))

    def test_app_burns_one_card_before_flop(self):
        """
        Tests that the app burns one card after dealing to
        players and before revealing the flop.
        """
        self.game.deal_flop()
        self.assertEqual(1, len(self.game.table.burner))

    def test_app_reveals_three_cards_for_flop(self):
        """Tests that the app reveals three cards for the flop."""
        self.game.deal_flop()
        self.assertEqual(Table.NUMBER_OF_CARDS_IN_FLOP, len(self.game.table.community_cards))

    def test_deal_called_multiple_time_increases_counter(self):
        """
        Tests that multiple calls to the deal method increase the round
        counter and perform different actions.
        """
        self.assertEqual(Game.INITIAL_DEAL_ROUND, self.game.round)
        self.game.deal()
        self.assertEqual(Game.FLOP_ROUND, self.game.round)
        self.game.deal()
        self.assertEqual(Game.TURN_ROUND, self.game.round)
        for player in self.game.players:
            self.assertEqual(Player.NUMBER_OF_CARDS_IN_PLAYERS_HAND, len(player.hand))
        self.assertEqual(Table.NUMBER_OF_CARDS_IN_FLOP, len(self.game.table.community_cards))
        self.assertEqual(1, len(self.game.table.burner))
