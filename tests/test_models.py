"""Application models tests."""
import unittest

from models import Card, Deck, Player, Table


class CardModelTest(unittest.TestCase):
    """ Class that handles the Card model unit tests."""

    def setUp(self):
        self.ace_of_spades = Card(rank='Ace', value=14, suit='Spades')
        self.jack_of_diamonds = Card(rank='Jack', value=11, suit='Diamonds')
        self.queen_of_hearts = Card(rank='Queen', value=12, suit='Hearts', is_face_up=True)

    def test_card_string_representation_face_up_is_displayed(self):
        """Tests if a given card face up is displayed as 'Rank of Suit', e.g. 'Queen of Hearts'."""
        self.assertEqual('Queen of Hearts', str(self.queen_of_hearts))

    def test_card_string_representation_face_down_is_hidden(self):
        """Tests if a given card face down is hidden, with a string representation of '??'."""
        self.assertEqual('??', str(self.jack_of_diamonds))

    def test_ace_is_greater_than_jack(self):
        """Tests if two cards can be compared by the '>' operator."""
        self.assertGreater(self.ace_of_spades, self.jack_of_diamonds)

    def test_jack_is_less_than_queen(self):
        """Tests if two cards can be compared by the '<' operator."""
        self.assertLess(self.jack_of_diamonds, self.queen_of_hearts)

    def test_two_jacks_are_equal(self):
        """Tests if two cards can be compared by the '==' operator."""
        self.assertEqual(Card(rank='Jack', value=11, suit='Clovers'), self.jack_of_diamonds)

    def test_jack_and_queen_are_different(self):
        """Tests if two cards can be compared by the '!=' operator."""
        self.assertNotEqual(self.jack_of_diamonds, self.queen_of_hearts)


class DeckModelTest(unittest.TestCase):
    """ Class that handles the Deck model unit tests."""
    NUMBER_OF_CARDS = 52
    SEED_1 = 123
    SEED_2 = 456

    def setUp(self):
        self.deck_1 = Deck(seed=self.SEED_1)
        self.deck_2 = Deck(seed=self.SEED_2)

    def test_newly_created_deck_has_52_cards(self):
        """Tests a newly created deck has the correct amount of playing cards."""
        self.assertEqual(self.NUMBER_OF_CARDS, len(self.deck_1))

    def test_decks_shuffled_with_different_seeds_are_different(self):
        """Tests two differently seeded decks end up differently shuffled."""
        self.assertNotEqual(self.deck_1.shuffle_cards(), self.deck_2.shuffle_cards())

    def test_decks_shuffled_with_same_seed_are_equal(self):
        """Tests two decks with the same seed end up shuffled the same way."""
        another_deck = Deck(seed=self.SEED_1)
        self.assertEqual(self.deck_1.shuffle_cards(), another_deck.shuffle_cards())

    def test_dealt_cards_are_removed_from_deck(self):
        """Tests that a given number of dealt cards are actually removed from the deck."""
        self.assertEqual(self.NUMBER_OF_CARDS, len(self.deck_1))
        card_amount = 5
        for _ in range(card_amount):
            self.deck_1.deal_card()
        self.assertEqual(self.NUMBER_OF_CARDS - card_amount, len(self.deck_1))


class PlayerModelTest(unittest.TestCase):
    """ Class that handles the Player model unit tests."""

    def setUp(self) -> None:
        self.player_1 = Player(name='Jean-Michel Doudoux', order=1)

    def test_player_string_represents_name(self):
        """Tests that the string representation of a player is their name attribute."""
        self.assertEqual('Jean-Michel Doudoux', str(self.player_1))

    def test_drawn_cards_are_added_to_players_hand(self):
        """Tests that the cards the player draws or is delt are added to their hand."""
        ace_of_spades = Card(rank='Ace', value=14, suit='Spades')
        self.player_1.draw_card(ace_of_spades)
        self.assertEqual(1, len(self.player_1.hand))
        self.assertEqual(ace_of_spades, self.player_1.hand[0])

    def test_drawn_cards_are_added_facing_up_to_human_players_hand(self):
        """Tests that the cards the player draws or is delt are added to their hand."""
        self.player_1.is_human = True
        ace_of_spades = Card(rank='Ace', value=14, suit='Spades')
        self.player_1.draw_card(ace_of_spades)
        self.assertEqual(1, len(self.player_1.hand))
        self.assertTrue(self.player_1.hand[0].is_face_up)


class TableModelTest(unittest.TestCase):
    """Class that handles the Table model unit tests."""

    def setUp(self):
        self.table = Table()
        self.ace_of_spades = Card(rank='Ace', value=14, suit='Spades')

    def test_burning_card(self):
        """Tests that a 'burnt' card goes to the table burner."""
        self.assertEqual(1, len(self.table.burn_card(self.ace_of_spades).burner))
        self.assertEqual(self.ace_of_spades, self.table.burner[0])

    def test_revealing_card(self):
        """Tests that a revealed card goes face up with the table community cards."""
        self.assertEqual(1, len(self.table.reveal_card(self.ace_of_spades).community_cards))
        self.assertEqual(self.ace_of_spades, self.table.community_cards[0])
        self.assertTrue(self.table.community_cards[0].is_face_up)
