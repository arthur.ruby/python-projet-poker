"""Module containing the Game controller."""
from models import Table, Deck, Player


class Game:
    """Represents a poker game with its players and deck of cards."""

    INITIAL_DEAL_ROUND = 0
    FLOP_ROUND = 1
    TURN_ROUND = 2

    def __init__(self, seed: int, player_name: str):
        """
        Represents a poker game with its players and deck of cards.

        :param seed: the seed used to pseudo-randomly shuffle the deck of cards.
        :param player_name: the human player's name.
        """
        self.seed = seed
        self.table = Table()
        self.deck = Deck(seed=seed)
        self.players = [
            Player(name="North", order=0),
            Player(name="East", order=1),
            Player(name=player_name, order=2, is_human=True),
            Player(name="West", order=3)
        ]
        self.round = self.INITIAL_DEAL_ROUND

    def shuffle_deck(self) -> Deck:
        """
        Shuffles the deck of cards.
        :return: the shuffled deck of cards.
        """
        return self.deck.shuffle_cards()

    def deal(self) -> None:
        """Deals cards according to the current round and increases it."""
        if self.round == self.INITIAL_DEAL_ROUND:
            self.deal_initial_round()
        elif self.round == self.FLOP_ROUND:
            self.deal_flop()
        self.round += 1

    def deal_initial_round(self):
        """Deals the initial round of cards to the players."""
        for _ in range(Player.NUMBER_OF_CARDS_IN_PLAYERS_HAND):
            for player in self.players:
                player.draw_card(self.deck.deal_card())

    def deal_flop(self):
        """Burns a card and reveals the flop cards on the table."""
        self.table.burn_card(card=self.deck.deal_card())
        for _ in range(Table.NUMBER_OF_CARDS_IN_FLOP):
            self.table.reveal_card(card=self.deck.deal_card())
