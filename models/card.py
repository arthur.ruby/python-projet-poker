"""Module containing the Card model."""


class Card:
    """Represents a playing card."""

    def __init__(self, rank: str, value: int, suit: str, is_face_up=False):
        """
        Represents a playing card.

        :param rank: the representation of the card (e.g. '8', 'Jack').
        :param value: the number of scoring points associated to the card (e.g. 8, 11).
        :param suit: the suit the card belongs to (e.g. 'Diamonds').
        :param is_face_up: true if the card is visible, false if it's face down.
        """
        self.rank = rank
        self.value = value
        self.suit = suit
        self.is_face_up = is_face_up

    def __gt__(self, other):
        return self.value > other.value

    def __lt__(self, other):
        return self.value < other.value

    def __eq__(self, other):
        return self.value == other.value

    def __str__(self):
        return f"{self.rank} of {self.suit}" if self.is_face_up else "??"
