"""Module containing the Player model."""
from models import Card


class Player:
    """Represents a poker player."""

    NUMBER_OF_CARDS_IN_PLAYERS_HAND = 2

    def __init__(self, name: str, order: int, is_human = False):
        """
        Represents a poker player.

        :param name: the player's name.
        :param order: player's turn in the game.
        :param is_human: true if the player is a real human beeing with feelings, false otherwise.
        """
        self.hand = []
        self.name = name
        self.order = order
        self.is_human = is_human

    def __str__(self):
        return self.name

    def draw_card(self, card: Card):
        """
        Method that allows a player to draw a card and add it to their hand.
        The card is set face up if the player is human or stays face down if it's A.I.

        :param card: a playing card from the deck.
        """
        card.is_face_up = self.is_human
        self.hand.append(card)
