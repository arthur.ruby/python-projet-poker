""" Main application entry point."""

import tkinter as tk

app = tk.Tk()

if __name__ == '__main__':
    app.mainloop()
